$(document).ready(function () {
    $('.galler').slick({
		dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
		autoplay:true,
		autoplaySpeed: 2000,
		draggable: true,
		pauseOnHover: true,
		pauseOnDotsHover: true,
        responsive: [
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });
});