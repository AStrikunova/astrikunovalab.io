let productSelector;
let productAmount;
let countingSection;
let radioSection;
let radioSec;
let optionSection;
let optionDelivery;
//
let prices = [1000, 1600, 1500];
let ableToSwitchFilling = [0, 1, 0];
let ableToDelivery = [0, 0, 1];
let fillingPrices = [200, 100, 150];
//
document.addEventListener("DOMContentLoaded", calc_init());
//
function calc_init(){
  productSelector = document.getElementById("product_select");// вид
  productAmount = document.getElementById("product_amount");// значение веса
  countingSection = document.getElementById("product_weight");// вес
  radioSection = document.getElementById("product_filling");// начинка
  radioSec = document.getElementsByName("filling_prop");
  optionSection = document.getElementById("product_delivery");// доставка
  optionDelivery = document.getElementById("chekDelivery");
  //
  countingSection.style.display = "none";
  radioSection.style.display = "none";
  optionSection.style.display = "none";
  //
  productSelector.addEventListener("change", calc_do);
  productSelector.addEventListener("change", calc_redisplay);
  optionDelivery.addEventListener("change", calc_do);
  productAmount.addEventListener("keyup", calc_do);
  //
  radioSec.forEach(function(item){
    item.addEventListener("change", calc_do);
  }
  );
  //
  console.log("init done");
}
function calc_redisplay(){
  let prod = productSelector.value;
  if(prod !== 0){
    countingSection.style.display = "inline-block";
    if(ableToSwitchFilling[prod - 1] == 1){
      radioSection.style.display = "inline-block";
    }
    else{
      radioSection.style.display = "none";
    }
    if(ableToDelivery[prod - 1] == 1){
      optionSection.style.display = "inline-block";
    }
    else{
      optionSection.style.display = "none";
    }
  }
  else{
    countingSection.display = "none";
    radioSection.style.display = "none";
    optionSection.style.display = "none";
    document.getElementById("price_counter").innerHTML = "0 руб.";
  }
}
function calc_do(){
  console.log("in calc_do");
  let prod = productSelector.value;
  let amount = productAmount.value;
  if(amount > 0 && amount < 1000000){
    let fillingMultiplier = 0;
    let delivery = 0;
    if(ableToSwitchFilling[prod - 1] == 1){
      radioSec = document.getElementsByName("filling_prop");
      for(let i = 0; i < radioSec.length; i++){
        if(radioSec[i].checked === true){
          fillingMultiplier = fillingPrices[i];
        }
      }
    }
    if(ableToDelivery[prod - 1] == 1 && optionDelivery.checked === true){
      delivery = 1240;
    }
    let sum = ((prices[prod - 1] + fillingMultiplier) * amount) + delivery;
    document.getElementById("price_counter").innerHTML = "" + sum + " руб.";
  }
  else{
    document.getElementById("price_counter").innerHTML = "0 руб.";
  }
}